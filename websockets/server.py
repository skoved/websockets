#!/usr/bin/env python

from flask import Flask
from flask_socketio import SocketIO
from pathlib import Path

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)

@app.route("/")
def hello_world():
    with open(f"{Path(__file__).parent.resolve()}/index.html") as page:
        return page.read()

@socketio.on('message')
def handle_message(data):
    print(f"received message: {data}")

@socketio.on('json')
def handle_json(json):
    print(f"received json: {json}")

@socketio.on('my event')
def handle_my_custom_event(arg1):
    print(f"received args: {arg1}")

if __name__ == "__main__":
    socketio.run(app)

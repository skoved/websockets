# websockets

A project to learn how websockets work using python.


## Getting started

### Install dependencies

```bash
poetry install
```

### Run the Web Server

run the following command:

```bash
poetry run flask --app websockets/server run
```
